"use strict";


!(function() {

    document.addEventListener("DOMContentLoaded", function(){
      
        const dateList = document.getElementById('image-menu');
        const img = document.getElementById('earth-image');
        const template = document.getElementById('image-menu-item');
        const imgTitle = document.getElementById('earth-image-title');
        const imgDate = document.getElementById('earth-image-date');

        const type = document.getElementById('type');
        type.addEventListener('change',  function() {
            getMostRecentDate(type.value);
        });
        const date = document.getElementById('date');
        
    
        let imageList = [];
    
        document.getElementById('request_form')
            .addEventListener("submit", function(e) {
                e.preventDefault();

                fetch(`https://epic.gsfc.nasa.gov/api/${type.value}/date/${date.value}`)
                    .then(response => {
                        if(!response.ok) {
                            throw new Error("Network response was not ok");
                        }
                        return response.json();
                    })
                    .then( data => {
                        if (data.length === 0) {
                           displayErrorMessage();
                        } else{
                        imageList = data;
                        displayImagesInMenu(imageList);
                        }
                    })
                    .catch ( error => {
                        console.log("Error fetching data:", error);
                    });
    
    
                    function displayImagesInMenu(images){
                        dateList.textContent = undefined;
                        images.forEach((image, index) => {
                            const menuItem = document.importNode(template.content, true);
                    
                            menuItem.querySelector('li').setAttribute('data-image-list-index', index);
                            menuItem.querySelector('.image-list-title').textContent = image.date;
                            menuItem.querySelector('li').addEventListener('click', function() {
                                displaySelectedImage(index);
                    
                                imgDate.textContent = image.date;
                                imgTitle.textContent = image.caption;
                            });
                    
                            dateList.appendChild(menuItem);
                        });
                    };
    
                    function displaySelectedImage(index){
                        let cutDate = date.value.split('-');
                        const selectedImageURL = imageList[index].image;
                        img.src = `https://epic.gsfc.nasa.gov/archive/${type.value}/${cutDate[0]}/${cutDate[1]}/${cutDate[2]}/jpg/${selectedImageURL}.jpg`;
                    };
            });

        function getMostRecentDate(typeValue){
            let dates = [];
            fetch(`https://epic.gsfc.nasa.gov/api/${typeValue}/all`)
                .then(response => {
                    if(!response.ok) {
                        throw new Error("Network response was not ok");
                    }
                    return response.json();
                })
                .then( data => {
                    dates = data;
                    dates.sort( (a, b) => b - a);
                    date.setAttribute('max', dates[0].date);
                    date.setAttribute('min', dates[dates.length-1].date);
                })
        };

        function displayErrorMessage(){
            img.src = undefined;
            dateList.textContent = undefined;
            const errorMessage =  document.importNode(template.content, true);
            const span = errorMessage.querySelector('span');
            span.parentNode.removeChild(span);
            errorMessage.children[0].textContent = 'No available images for this date';
            dateList.appendChild(errorMessage);

            imgDate.textContent = undefined;
            imgTitle.textContent = undefined;
        }
    });
}());

